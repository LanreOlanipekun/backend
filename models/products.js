"use strict";
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const productsSchema = mongoose.Schema({
    _id: ObjectId,
    name:{
        required: true,
        type: String
    },
    description: {
        required: true,
        type: String
    },
    price: {
        required: true,
        type: Number
    },
    category: {
        required: true,
        type: String
    },
    image:{
        required: true,
        type: String
    },
    color: {
        required: true,
        type: String
    },
    Date: {
        type: Date, 
        default: Date.now
    }
})

module.exports = mongoose.model('Products', productsSchema)