// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

chai.should();

chai.use(chaiHttp);

describe('/product', () => {
    it('it should get all products', (done) => {
        const user = {
            username: 'nipek',
            password: 'nipek',
        };
        chai.request(server)
            .get('/products')
            .send(user)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                //   res.body.should.have.property('token');
                //   res.body.should.have.property('success').eql(true);
                done();
            });
    });
    it('it should get a product with invalid product id', (done) => {
        const user = {
            username: 'nipek',
            password: 'nipek',
        };
        chai.request(server)
            .get('/products/5a946c3fd383ad4dbd485c61')
            .send(user)
            .end((err, res) => {
                res.should.have.status(404);
                res.body.should.be.a('object');
                //   res.body.should.have.property('token');
                res.body.should.have.property('status').eql('error');
                done();
            });
    });

    // it('it should create a product', (done) => {
    //     chai.request(server)
    //         .post('/products')
    //         .set('content-type', 'application/x-www-form-urlencoded')
    //         .field('name', 'customValue')
    //         .field('name', 'customValue')

    //         .field('description', 'customValue')

    //         .field('price', 'customValue')

    //         .field('category', 'customValue')
    //         .field('color', '#ffffff')

    //         .attach('files', './hap.jpg', 'hap.jpg')
    //         .end((err, res) => {
    //             res.should.have.status(201);
    //             res.body.should.be.a('object');
    //             //   res.body.should.have.property('token');
    //             res.body.should.have.property('status').eql('success');
    //             done();
    //         });
    // });
});

