"use strict";
const express = require('express');
const router = express.Router();
const multer = require('multer');
const productController = require('../controller/products')


const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads');
    },
    filename: function(req, file, cb) {
      const now = new Date().toISOString();
      const date = now.replace(/:/g, '-');
      cb(null, date + file.originalname);
    }
  });
  
  const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
      cb(null, true);
    } else {
      cb(null, false);
    }
  };
  
  const upload = multer({ storage: storage,
    limits: {
    fileSize: 1024 * 1024 * 5
  },
    fileFilter: fileFilter
   });

// get all products
router.get('/', productController.get_all_products)

// get single product
router.get('/:id', productController.get_single_product)

// create product
router.post('/', upload.single('image'), productController.create_product)


module.exports = router;