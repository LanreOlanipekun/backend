"use strict";
const http = require('http');
const config = require('./config')
const app = require('./app')
const port = config.port;
const server = http.createServer(app);


// db.connect()
// .then(()=> {
//    server.listen(port, () => console.log(`App running on port ${port}`))
// })
server.listen(port, () => console.log(`App running on port ${port}`))

module.exports = server

